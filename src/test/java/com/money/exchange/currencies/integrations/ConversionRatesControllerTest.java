package com.money.exchange.currencies.integrations;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.money.exchange.currencies.enums.ConversionRequestType;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ConversionRatesControllerTest {

	private static final String CONVERSION_RATES_API_ENDPOINT = "/currencies/EUR/conversion-rates";
	@Autowired
	MockMvc mockMvc;

	@Test
	public void getConversionRatesTestLatestAvailableRate() throws Exception {
		mockMvc.perform(get(CONVERSION_RATES_API_ENDPOINT).param("type", ConversionRequestType.LATEST.toString())).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("USD")));
		;
	}

	@Test
	public void getConversionRatesTestNotImplementedCurrecnyCode() throws Exception {
		String notImplementedCurrencyCode = "/currencies/USD/conversion-rates";
		mockMvc.perform(get(notImplementedCurrencyCode).param("type", ConversionRequestType.LATEST.toString())).andDo(print())
				.andExpect(status().isNotImplemented());
	}

	@Test
	public void getConversionRatesTestBadRequest() throws Exception {
		String notImplementedCurrencyCode = "/currencies/XXX/conversion-rates";
		mockMvc.perform(get(notImplementedCurrencyCode).param("type", ConversionRequestType.LATEST.toString())).andDo(print())
				.andExpect(status().isBadRequest());
	}

	@Test
	public void getConversionRatesTestHistoricalRates() throws Exception {
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.set("type", ConversionRequestType.HISTORICAL.toString());
		params.set("from", LocalDateTime.now().minusMinutes(10).toString());
		params.set("to", LocalDateTime.now().toString());
		mockMvc.perform(get(CONVERSION_RATES_API_ENDPOINT).params(params)).andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void getConversionRatesTestHistoricalRatesBadRequest() throws Exception {
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.set("type", ConversionRequestType.HISTORICAL.toString());
		mockMvc.perform(get(CONVERSION_RATES_API_ENDPOINT).params(params)).andDo(print()).andExpect(status().isBadRequest());
	}

}

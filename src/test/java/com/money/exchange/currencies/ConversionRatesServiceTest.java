package com.money.exchange.currencies;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.money.exchange.currencies.conversionrates.ConversionRate;
import com.money.exchange.currencies.conversionrates.ConversionRateRepository;
import com.money.exchange.currencies.conversionrates.ConversionRateService;
import com.money.exchange.currencies.enums.CurrencyCode;
import com.money.exchange.rest.Rate;
import com.money.exchange.rest.services.RateService;

@RunWith(MockitoJUnitRunner.class)
public class ConversionRatesServiceTest {
	@Mock
	private RateService ratesService;

	@Mock
	private ConversionRateRepository ratesRepository;

	@InjectMocks
	private ConversionRateService serviceUnderTest;

	@Test
	public void ingestCurrencyRatesTestSaveRates() {

		when(ratesService.getRates(CurrencyCode.EUR)).thenReturn(dummyRate());
		when(ratesRepository.save(any(ConversionRate.class))).thenReturn(new ConversionRate());

		serviceUnderTest.ingestCurrencyRates();

		verify(ratesRepository, times(1)).save(any(ConversionRate.class));
	}

	private Rate dummyRate() {
		Rate rate = new Rate();
		rate.setBase("EUR");
		rate.setDate(LocalDate.now());
		Map<String, BigDecimal> ratesMap = new HashMap<>();
		ratesMap.put("USD", new BigDecimal(0.81));
		rate.setRates(ratesMap);
		return rate;
	}

}

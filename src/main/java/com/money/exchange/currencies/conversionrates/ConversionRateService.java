package com.money.exchange.currencies.conversionrates;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.money.exchange.currencies.enums.CurrencyCode;
import com.money.exchange.rest.Rate;
import com.money.exchange.rest.services.RateService;

@Service
public class ConversionRateService {

	@Autowired
	private RateService ratesService;

	@Autowired
	ConversionRateRepository ratesRepository;

	@Scheduled(fixedDelayString = "${getRates.delay.time}")
	public void ingestCurrencyRates() {
		Rate rateResponse = ratesService.getRates(CurrencyCode.EUR);
		ConversionRate cRates = new ConversionRate();
		cRates.setRate(rateResponse.getRates().get(CurrencyCode.USD.toString()));
		cRates.setAt(LocalDateTime.now());
		cRates.setCurrencyCode(CurrencyCode.USD);
		ratesRepository.save(cRates);
	}

	public ConversionRate getLastestRate() {
		return ratesRepository.findTop1ByOrderByAtDesc();
	}

	public List<ConversionRate> getHistoricalRates(LocalDateTime from, LocalDateTime to) {
		return ratesRepository.findAllByAtBetween(from, to);
	}
}

package com.money.exchange.currencies.conversionrates;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConversionRateRepository extends JpaRepository<ConversionRate, Serializable> {

	public ConversionRate findTop1ByOrderByAtDesc();

	public List<ConversionRate> findAllByAtBetween(LocalDateTime from, LocalDateTime to);
}

package com.money.exchange.currencies.enums;

public enum CurrencyCode {
	EUR, USD
}

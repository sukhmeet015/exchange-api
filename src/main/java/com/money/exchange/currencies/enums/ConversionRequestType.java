package com.money.exchange.currencies.enums;

public enum ConversionRequestType {
	LATEST, HISTORICAL
}

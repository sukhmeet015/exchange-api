package com.money.exchange.currencies;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.money.exchange.config.CaseInsensitivePropertyEditor;
import com.money.exchange.currencies.conversionrates.ConversionRate;
import com.money.exchange.currencies.conversionrates.ConversionRateService;
import com.money.exchange.currencies.enums.ConversionRequestType;
import com.money.exchange.currencies.enums.CurrencyCode;
import com.money.exchange.exceptions.BadRequestException;
import com.money.exchange.exceptions.NotImplementedExcepiton;

@RestController
@RequestMapping("/currencies")
public class CurrenciesController {

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(CurrencyCode.class, new CaseInsensitivePropertyEditor());
	}

	@Autowired
	ConversionRateService conversionRate;

	@GetMapping("{currencyCode}/conversion-rates")
	public ResponseEntity<?> getConversionRates(@PathVariable CurrencyCode currencyCode, @RequestParam ConversionRequestType type,
			@RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime from,
			@RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to)
			throws NotImplementedExcepiton, BadRequestException {

		if (currencyCode != CurrencyCode.EUR) {
			throw new NotImplementedExcepiton("Only EUR conversion rates are supported at moment, please select EUR as currency");
		}
		if (type == ConversionRequestType.LATEST) {
			ConversionRate latestConversionRate = conversionRate.getLastestRate();
			return ResponseEntity.ok(latestConversionRate);
		} else {
			List<ConversionRate> historicalConversionRates = getHistoricalConversionRate(from, to);
			return ResponseEntity.ok(historicalConversionRates);
		}

	}

	private List<ConversionRate> getHistoricalConversionRate(LocalDateTime from, LocalDateTime to) throws BadRequestException {
		if (from == null) {
			throw new BadRequestException("from value should not be empty");
		}
		if (to == null) {
			to = LocalDateTime.now();
		}
		List<ConversionRate> historicalConversionRates = conversionRate.getHistoricalRates(from, to);
		return historicalConversionRates;
	}

}

package com.money.exchange.config;

import java.beans.PropertyEditorSupport;

import com.money.exchange.currencies.enums.CurrencyCode;

public class CaseInsensitivePropertyEditor extends PropertyEditorSupport {

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		CurrencyCode upper = CurrencyCode.valueOf(text);
		setValue(upper);
	}
}
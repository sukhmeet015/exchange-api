package com.money.exchange.exceptions;

import java.io.IOException;
import java.time.ZonedDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandlingController extends ResponseEntityExceptionHandler {

	private static final Logger log = LoggerFactory.getLogger(ExceptionHandlingController.class);

	private static final String GENERIC_ERROR_MSG = "There was an internal error. Error code %s, timestamp %s";

	private HttpStatus getExceptionResponseStatus(Exception e) {
		ResponseStatus annotation = AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class);
		return (annotation != null) ? annotation.value() : null;
	}

	private String getGenericErrorMessage() {
		String correlationId = MDC.get("correlation-id");
		return String.format(GENERIC_ERROR_MSG, correlationId, ZonedDateTime.now());
	}

	private ResponseEntity<Object> handle(WebRequest request, Exception exception, HttpStatus status) {
		return handle(request, exception, status, null);
	}

	private ResponseEntity<Object> handle(WebRequest request, Exception exception, HttpStatus status, String customErrorMessage) {
		HttpStatus returnStatus;
		if (status != null) {
			returnStatus = status;
		} else {
			returnStatus = getExceptionResponseStatus(exception);
		}
		log(exception, request);

		return new ResponseEntity<>(exception.getMessage(), returnStatus);
	}

	private void log(Exception e, WebRequest request) {
		ServletWebRequest servletWebRequest = (ServletWebRequest) request;
		log.error(e.getMessage() + ", request: " + servletWebRequest.getRequest().getMethod() + " " + servletWebRequest.getRequest().getRequestURI(), e);
	}

	@ResponseBody
	@ExceptionHandler(BadRequestException.class)
	ResponseEntity<Object> handleBadRequestException(WebRequest request, BadRequestException exception) {
		return handle(request, exception, HttpStatus.BAD_REQUEST);
	}

	@ResponseBody
	@ExceptionHandler(IOException.class)
	ResponseEntity<Object> handleIOException(WebRequest request, IOException exception) {
		return handle(request, exception, HttpStatus.INTERNAL_SERVER_ERROR, getGenericErrorMessage());
	}

	@ResponseBody
	@ExceptionHandler(RuntimeException.class)
	ResponseEntity<Object> handleRuntimeException(WebRequest request, RuntimeException exception) {
		return handle(request, exception, HttpStatus.INTERNAL_SERVER_ERROR, getGenericErrorMessage());
	}

}

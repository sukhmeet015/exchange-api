package com.money.exchange.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_IMPLEMENTED)
public class NotImplementedExcepiton extends Exception {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public NotImplementedExcepiton() {
		super();
	}

	public NotImplementedExcepiton(String message) {
		super(message);
	}

}

package com.money.exchange.rest.services;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.money.exchange.currencies.enums.CurrencyCode;
import com.money.exchange.rest.Rate;

@Service
public class RateService {
	@Value("${freeConversionAPIURL}")
	private String converAPIURL;

	@Autowired
	private RestTemplate restTemplate;

	@Retryable(value = { RuntimeException.class }, maxAttempts = 3, backoff = @Backoff(delay = 5000))
	public Rate getRates(CurrencyCode base) {
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
		queryParams.add("base", base.toString());
		try {
			return Optional
					.of(restTemplate.getForObject(UriComponentsBuilder.fromUri(new URI(converAPIURL)).queryParams(queryParams).build().toString(), Rate.class))
					.orElseThrow(RuntimeException::new);
		} catch (RestClientException | URISyntaxException e) {
			throw new RuntimeException(e);
		}

	}

}

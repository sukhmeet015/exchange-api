package com.money.exchange.rest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;

public class Rate implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String base;
	private LocalDate date;
	private Map<String, BigDecimal> rates;

	// Getter Methods

	public String getBase() {
		return base;
	}

	public LocalDate getDate() {
		return date;
	}

	// Setter Methods

	public void setBase(String base) {
		this.base = base;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Map<String, BigDecimal> getRates() {
		return rates;
	}

	public void setRates(Map<String, BigDecimal> rates) {
		this.rates = rates;
	}

}

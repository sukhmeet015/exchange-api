Followings are the point regarding exchange api:
1.	This is maven project with springboot
2.	There is one endpoint GET /currencies/{currencyCode}/conversion-rates 
a.	Type parameter define what type of conversion-rates of particular currency you need.
b.	This endpoint returns result by quering local in memory database.
3.	The api contains ingestion method which is consuming a public api for converstion rates and storing them in the in memory database and data is accessed at interval specified in application properties file by spring scheduling.
4.	I used spring retry to retry if external public api is down for some time.
5.	I have implemented swagger and it will accessed on http://127.0.0.1:8080/swagger-ui.html#!  After starting the service.
